;; tessa hall's emacs configuration
;; for network troubleshooting uncomment below:
;; (setq gnutls-log-level 2)
;; package repo configuration

;; Don't cry wolf
(setq warning-minimum-level :emergency)

(package-initialize)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                     ("melpa" . "https://melpa.org/packages/")))
;; load all the packages
(defvar prelude-packages
  '(tramp
		lsp-mode
		smart-mode-line
		use-package
		;; scala
		scala-mode sbt-mode lsp-metals
		;; java
		lsp-java mvn
		;; haskell
		lsp-ui lsp-haskell
		;; LaTeX
		;; other languages
		csharp-mode
		latex-preview-pane
		;; web-dev
		markdown-mode
		;; themes
		monokai-theme gruvbox-theme cyberpunk-theme color-theme-sanityinc-solarized
		srcery-theme lush-theme dracula-theme
		;; helm
		helm helm-tramp helm-themes helm-company helm-lsp helm-hoogle
		;; evil
		evil
		which-key
		tabspaces
		company company-cabal company-ghci company-native-complete
		flycheck
		yasnippet
		magit
		load-dir
		rainbow-delimiters
		cheatsheet
		elfeed
		hamburger-menu
		no-littering
		shell-here
		pacfiles-mode
		vundo
		)
  "A list of packages to ensure are installed at launch.")

(defun prelude-packages-installed-p ()
  (cl-loop for p in prelude-packages
					 when (not (package-installed-p p)) do (cl-return nil)
					 finally (cl-return t)))

(unless (prelude-packages-installed-p)
  ;; check for new packages (package versions)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ;; install the missing packages
  (dolist (p prelude-packages)
    (when (not (package-installed-p p))
      (package-install p))))

(provide 'prelude-packages)

;;;; prelude-packages.el ends here (borrowed from prelude)
;;;; Here is most of my original configuration

;; no littering backup and auto-save files!!!
(require 'no-littering)
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))


;;; load all elusp files from config/lisp directory
(require 'load-dir)
(load-dir-one "~/.emacs.d/lisp")

;;; mode hooks all listed out for clarity
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook 'hl-line-mode)
(add-hook 'prog-mode-hook (lambda () (setq truncate-lines t)))
(add-hook 'prog-mode-hook 'show-paren-mode)
(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)
(add-hook 'latex-mode-hook 'display-line-numbers-mode)
(add-to-list 'auto-mode-alist '("\\.tex\\'" . latex-mode))
;; builtin relative line numbering with prog-mode hook
(setq-default display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;;; all global minor-modes to enable/disable on startup
(evil-mode 1)
(helm-mode 1)
(electric-pair-mode 1)
(delete-selection-mode 1)
(which-function-mode 1)
(which-key-mode 1)
(tabspaces-mode 1)

;;; reduce prompts to single letters
(fset 'yes-or-no-p 'y-or-n-p)

;; turn off gross gui stuff
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
;; add hamburger-menu to modeline, instead of menu above tab-bar
(require 'hamburger-menu)
(setq mode-line-front-space 'hamburger-menu-mode-line)

;; smart-mode-line
(setq sml/no-confirm-load-theme t)
(sml/setup)

;;; default theme
(add-hook 'after-init-hook (lambda () (load-theme 'leuven-dark)))
(set-frame-font "Iosevka Term SS05-12")
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-string-face ((t (:foreground "#b8bb26" :slant italic :family "Victor Mono"))))
 '(tab-bar ((t (:background "#32302f" :foreground "#fdf4c1" :slant italic :weight semi-bold :height 1.12 :family "Victor Mono"))))
 '(tab-bar-tab-inactive ((t (:background "#32302f" :foreground "#fdf4c1" :weight normal)))))

;;; Keyboard Configuration
;; M-y to cycle through elements in the kill-ring with helm (clipboard)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
;; M-x to open the helm
(global-set-key (kbd "M-x") 'helm-M-x)
;; M-u to show undo tree
(global-set-key (kbd "M-u") 'vundo)
;; M-h to show generic help (otherwise C-h is the prefix following anything else, still)
(global-set-key (kbd "M-h") 'help)
;; C-h to backward-kill-word (since most terminals interpret Ctrl+Backspace as C-h)
(global-set-key (kbd "C-h") 'backward-kill-word)
;; C-x g to open the magit window
(global-set-key (kbd "C-x g") 'magit-status)
;; C-x f to find the definition of what is under the cursor
(global-set-key (kbd "C-x f") 'xref-find-definitions)
;; let helm control the find-file function (C-x f)
(define-key global-map [remap find-file] 'helm-find-files)
;; window controls:
(global-set-key (kbd "<M-up>") 'shrink-window)
(global-set-key (kbd "<M-down>") 'enlarge-window)
(global-set-key (kbd "<M-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<M-right>") 'enlarge-window-horizontally)

(defun my-sh-send-command (command)
  "Send COMMAND to current shell process.

    Creates new shell process if none exists.

    See URL `https://stackoverflow.com/a/7053298/5065796'"
  (let ((proc (get-process "shell"))
        pbuf)
    (unless proc
      (let ((currbuff (current-buffer)))
        (shell)
        (switch-to-buffer currbuff)
        (setq proc (get-process "shell"))
        ))
    (setq pbuff (process-buffer proc))
    (setq command-and-go (concat command "\n"))
    (with-current-buffer pbuff
			(goto-char (process-mark proc))
      (insert command-and-go)
      (move-marker (process-mark proc) (point))
      )
    (process-send-string proc command-and-go)))

(defun my-buffer-file-java ()
  "Send current buffer file to shell as java call."
  (interactive)
  (my-sh-send-command (concat "java " (buffer-file-name))))

(global-set-key (kbd "C-x j") 'my-buffer-file-java)


;; tab settings:
'(tab-bar-close-last-tab-choice 'delete-frame)
'(tab-bar-mode t)
'(tab-bar-position t)
'(tab-bar-select-tab-modifiers '(meta))
'(tab-bar-tab-hints t)
'(tab-bar-tab-name-function 'tab-bar-tab-name-current-with-count)
'(tab-bar-tab-name-truncated-max 30)

;; Custom windowing behaviors related to tab-bar
;; Make magit buffer open in a new tab by default
(add-to-list 'display-buffer-alist
   '("magit:+"
     (display-buffer-in-tab)))

;;; miscellanious settings
;; Keep auto-save/backup files separate from source code:  https://github.com/scalameta/metals/issues/1027
(setq-default auto-mode-alist (cons '("\\.scala$" . scala-mode) auto-mode-alist)
							ediff-window-setup-function 'ediff-setup-windows-plain
							x-select-enable-clipboard-manager nil
							helm-home-url "https://www.duckduckgo.com"
							evil-emacs-state-cursor (quote ("#D50000" hbar))
							evil-insert-state-cursor (quote ("#D50000" bar))
							evil-normal-state-cursor (quote ("#F57F17" box))
							evil-visual-state-cursor (quote ("#66BB6A" box))
							tab-width 2 ;; indentation needs to be less intense these days, jeez
							initial-buffer-choice "~/Documents" ;; just my preference
							ring-bell-function 'ignore
							show-trailing-whitespace t
							indicate-empty-lines t
							show-paren-delay 0
							backup-by-copying t
							sentence-end-double-space nil
							tab-always-indent 'complete
							eshell-buffer-shorthand t
							matrix-client-show-images t
							lsp-prefer-flymake nil
							latex-run-command "pdflatex"
							use-package-always-defer t
							use-package-always-ensure t
							backup-directory-alist `((".*" . ,temporary-file-directory))
							auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;;; special behaviour for non-graphical mode (overrides default settings)
(when (not window-system)
	(setq-default initial-buffer-choice nil
								matrix-client-show-images nil)
	;; other special behaviours, potentially
	)

;; Custom warning suppression of spurious warnings by docstrings and such
(add-to-list 'warning-suppress-log-types '(unlock-file))
(add-to-list 'warning-suppress-types '(unlock-file))

;;; eshell customization
(require 'eshell)
;; alias notes:
;; top aliased to proced
;; diff aliased to ediff-files
;; check eshell/alias for more
(defvar eshell-visual-commands
	'("vi" "vim"										      ; what is going on??
		"screen"                            ; ok, a valid program...
		"less" "more"                       ; M-x view-file
		"lynx" "ncftp"                      ; w3.el, ange-ftp
		"pine" "tin" "trn" "elm"						; GNUS!!
		)           )
;; I use proced instead of top: here are some good defaults
(require 'proced)
(proced-toggle-auto-update 1)
(setq proced-auto-update-interval 1)
;;; Things to consider adding:
;; a dedicated cheatsheet for my config's special keybinds:
;; using: https://github.com/darksmile/cheatsheet
(require 'my-cheats)
;; cheat template

;; metals snippet
(require 'use-package)

;; Enable scala-mode for highlighting, indentation and motion commands
(use-package scala-mode
  :mode "\\.s\\(cala\\|bt\\)$")

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
)

;; Enable nice rendering of diagnostics like compile errors.
(use-package flycheck
  :init (global-flycheck-mode))

(use-package lsp-mode
  ;; Optional - enable lsp-mode automatically in scala files
  :hook  (scala-mode . lsp)
         (lsp-mode . lsp-lens-mode)
  :config (setq lsp-prefer-flymake nil))

;; Enable nice rendering of documentation on hover
(use-package lsp-ui)

;; lsp-mode supports snippets, but in order for them to work you need to use yasnippet
;; If you don't want to use snippets set lsp-enable-snippet to nil in your lsp-mode settings
;;   to avoid odd behavior with snippets and indentation
(use-package yasnippet)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
	 '(cheatsheet color-theme-sanityinc-solarized company-cabal
								company-ghci company-native-complete cyberpunk-theme
								dracula-theme elfeed evil flycheck gruvbox-theme
								hamburger-menu helm-company helm-hoogle helm-lsp
								helm-themes helm-tramp latex-preview-pane load-dir
								lsp-haskell lsp-java lsp-metals lsp-ui lush-theme
								magit monokai-theme mvn no-littering pacfiles-mode
								plantuml-mode rainbow-delimiters sbt-mode shell-here
								smart-mode-line srcery-theme tabspaces undo-tree vundo
								which-key yasnippet)))
