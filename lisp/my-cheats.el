(require 'cheatsheet)
(cheatsheet-add-group 'Common
											'(:key "M-x shell-here" :description "Open a shell in buffer's current directory. Works in TRAMP.")
											'(:key "C-x C-f /ssh:user@host:" :description "Connect emacs to remote host with TRAMP and get a directory listing."))

(cheatsheet-add-group 'Control
                      '(:key "C-x C-f" :description "Find and open a file.")
                      '(:key "C-x f" :description "Find the definition of the symbol/variable/function under the pointer if applicable.")
											'(:key "C-x g" :description "Open magit to manige git.")
											'(:key "C-x +" :description "Equalize all windows.")
											'(:key "C-x j" :description "Run file of current buffer with java.")
											'(:key "C-h" :description "Delete previous word under cursor (this is usually Control+Backspace in terminals).")
											'(:key "C-z" :description "Toggle evil-mode (vi-keyibinds)."))

(cheatsheet-add-group 'Meta
                      '(:key "M-x" :description "Run emacs command.")
                      '(:key "M-u" :description "Show undo tree.")
                      '(:key "M-h" :description "Emacs help.")
											'(:key "M-y" :description "Show clipboard history.")
											'(:key "M-up" :description "Resize window up.")
											'(:key "M-down" :description "Resize window down.")
											'(:key "M-left" :description "Resize window left.")
											'(:key "M-right" :description "Resize window right."))

(provide 'my-cheats)
