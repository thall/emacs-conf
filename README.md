# emacs-conf
Tessa Hall's emacs configuration. See "special keybinds" and "general notes" for an outline of what is included.
The list of modified keybindings is available for quick reference from within emacs once the config is installed
by executing `M-x cheatsheet-show` to open the "cheatsheet" buffer.

## Requirements:
- `emacs>=26`
- victor mono font
- iosevka ss05 font

## Optional Dependencies:
- `sbt`
- `metals-emacs` language server for scala development
- `ghostscript` and texlive packages for your system (`texlive-most` and `biber` for Arch-Linux based distros)
- `haskell-language-server` running in your haskell project directory
- maven (`mvn`)
- JDK 17+ for java development (shouldn't apply to Scala if you use sbt)

## Setup:
- run `git clone https://framagit.org/thall/emacs-conf.git` in your home directory
- run `mv emacs-conf/ .emacs.d/` in your home directory
- launch `emacs` or `emacs -nw` and wait for the initial automatic setup to finish

## Special Keybinds:
- `evil-mode` is the default, so all vi keybinds apply
- `C-z` toggles `evil-mode` and switches back to regular emacs keybindings in addition to the ones below
- `C-x C-f` - `helm-find-files` (this is done globally; so anything invoking `find-file` is fed to `helm-find-files`
- `C-x f` - `xref-find-definitions` find the definition of the function or value under the cursor if applicable
- `C-x g` - `magit-status` open magit status buffer for the git repository of the file currently opened
- `C-x j` - A custom function to run `java` on the file of current buffer within `shell`. Useful for jdk17+ on files with a main.
- `M-x` - `helm-M-x` let helm handle `M-x` for fuzzy-completion and history
- `M-y` - `helm-show-kill-ring` look through the elements stored in the kill-ring (emacs clipboard, essentially)
- `M-u` - `undo-tree-visualize` handle undos in the traditional emacs fashion, but with a visual aid
- `M-<direction-key>` - resize windows
- `C-h` - `backward-kill-word` deletes the last word behind the cursor (most terminals interpret Ctrl+Backspace as ^C-h and C-Backspace still works by default)
- `M-h` - `help` opens the generic help buffer for emacs (normally `C-h` but this works better in terminal emulators) (`C-h` is still help-suffix when not standalone)

## General Notes:
A handful of themes installed by default for a good selection of dark and light, they can be cycled through with `M-x helm-themes`.
Evil mode is enabled, all programming modes have relative line numbers, and my default buffer is set to dired ~/Documents.
Tramp is setup with helm, so remote work is made easy. `which-key-mode` will suggest completions to key combos.
When emacs does not have graphics (like with `emacs -nw`) no default buffer is opened, only files passed as arguments.
In all modes something to note is the modeline uses `which-function-mode` to display the name of the function the
cursor is currently within when the programming mode supports it.
